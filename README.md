# meta-prpl
This is common layer for all prpl metalayers:
    1. meta-prplmesh
    2. meta-amx
    3. meta-lcm

## Build
Turris Omnia:

1. Initialize the Repo tool with the desired manifest:

    ```
    repo init -u https://gitlab.com/prpl-foundation/prplrdkb/prplrdkb -b master -m rdkb-turris-extsrc.xml
    ```

2. Sync the repositories:

    ```
    repo sync -j$(nproc) --no-clone-bundle
    ```

    This command will fetch the repositories specified in the manifest and bring your local workspace up to date with the remote repositories.

3. Source build env

    ```
    MACHINE=turris source meta-prpl/setup-environment
    ```

4. Run image build

    ```
    bitbake rdk-generic-broadband-image
    ```